import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException {
        Matriz orig = new Matriz(3, 2);
        Matriz m1 = new Matriz(2,2);
        Matriz m2 = new Matriz(2,2);
        Matriz mi1 = new Matriz(3,5);
        Matriz mi2 = new Matriz(2,4);
        Matriz soma;
        Matriz multi;
        Matriz somainva;
        Matriz prodinva;

        
        System.out.println("Matriz original 1: " + m1);
        System.out.println("Matriz original 2: " + m2);
        soma=m1.soma(m2);
        System.out.println("Matrizes somadas: " + soma);
        multi=m1.prod(m2);
        System.out.println("Matriz multiplicadas: " + multi);
        somainva=mi1.soma(mi2);
        System.out.println("Matrizes somadas: " + somainva);
        prodinva=mi1.prod(mi2);
        System.out.println("Matriz multiplicadas: " + prodinva);
        
    }
}