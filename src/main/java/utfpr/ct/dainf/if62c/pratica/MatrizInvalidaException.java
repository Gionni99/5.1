package utfpr.ct.dainf.if62c.pratica;

/**
 * @author Giovanni Bandeira
 */
public class MatrizInvalidaException extends Exception{
    int numColunas,numLinhas;

    public MatrizInvalidaException(int numColunas,int numLinhas){
        super(String.format("Matriz de %dX%d nao pode ser criada",numColunas,numLinhas));
        this.numColunas=numColunas;
        this.numLinhas=numLinhas;
    }
    
    public int getNumColunas() {
        return numColunas;
    }

    public int getNumLinhas() {
        return numLinhas;
    }
    
}
